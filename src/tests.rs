use super::run;
use rocket::http::{Header, Status};
use rocket::local::blocking::Client;

fn client_tracked() -> Client {
    let args = vec!["tiles-proxy".to_string(), "tiles-proxy.toml".to_string()];
    let run_result = run(args);
    assert!(
        run_result.is_ok(),
        "Running with configuration must succeed. Error: {}",
        run_result.err().unwrap_or("No error".to_string())
    );
    let rocket_instance = run_result.unwrap();
    Client::tracked(rocket_instance).unwrap()
}

#[test]
fn missing_configuration() {
    let res = run(Vec::new());
    assert!(
        res.is_err(),
        "Running without argument for the configuration file must fail."
    );
}

#[test]
fn xyz() {
    let client = client_tracked();
    let response = client.get("/xyz/piano-fr/a/6/35/22.png").dispatch();
    assert_eq!(
        response.status(),
        Status::Ok,
        "A tile from piano-fr must be retrieved."
    );
    let etag_header = response.headers().get_one("ETag");
    assert!(
        etag_header.is_some(),
        "The ETag HTTP header must be present."
    );
    let etag_value = format!("{}", etag_header.unwrap_or(""));
    assert_ne!(
        "", etag_value,
        "The value of the ETag HTTP header must not be blank."
    );

    let client2 = client_tracked();
    let response2 = client2
        .get("/xyz/piano-fr/a/6/35/22.png")
        .header(Header::new("If-None-Match", format!(r#""{}""#, etag_value)))
        .dispatch();
    assert_eq!(
        response2.status(),
        Status::NotModified,
        "Using the ETag header 'If-None-Match' must return 304. Returned: {}",
        response2.status()
    );
}
