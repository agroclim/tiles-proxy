//! Structs to store HTTP requests of a tile.

use std::path::Path;
use std::path::PathBuf;
use std::str::FromStr;

pub trait TileRequest: Send + Sync {
    fn dirpath(&self, base_dir: &str) -> PathBuf;
    fn filepath(&self, base_dir: &str) -> PathBuf;
    fn get_alias(&self) -> &str;
}

#[derive(Debug, PartialEq, FromForm)]
pub struct WmtsRequest {
    #[field(default = "")]
    pub alias: String,
    #[field(default = None)]
    pub layer: String,
    #[field(default = None)]
    pub style: String,
    #[field(default = None)]
    pub tilematrixset: String,
    #[field(name = "Service", default = None)]
    pub service: String,
    #[field(name = "Request", default = None)]
    pub request: String,
    #[field(name = "Version", default = None)]
    pub version: String,
    #[field(name = "Format", default = None)]
    pub format: String,
    #[field(name = "TileMatrix", default = None)]
    pub tile_matrix: String,
    #[field(name = "TileCol", default = None)]
    pub tile_col: String,
    #[field(name = "TileRow", default = None)]
    pub tile_row: String,
}

impl WmtsRequest {
    pub fn with_alias(&self, alias: &str) -> Self {
        WmtsRequest {
            alias: String::from_str(alias).expect(""),
            layer: String::from(self.layer.as_str()),
            style: String::from(self.style.as_str()),
            tilematrixset: String::from(self.tilematrixset.as_str()),
            service: String::from(self.service.as_str()),
            request: String::from(self.request.as_str()),
            version: String::from(self.version.as_str()),
            format: String::from(self.format.as_str()),
            tile_matrix: String::from(self.tile_matrix.as_str()),
            tile_col: String::from(self.tile_col.as_str()),
            tile_row: String::from(self.tile_row.as_str()),
        }
    }
}

impl TileRequest for WmtsRequest {
    fn dirpath(&self, base_dir: &str) -> PathBuf {
        Path::new(base_dir)
            .join(self.get_alias())
            .join(self.layer.as_str())
            .join(self.style.as_str())
            .join(self.tilematrixset.as_str())
            .join(self.service.as_str())
            .join(self.request.as_str())
            .join(self.version.as_str())
            .join(self.format.as_str())
            .join(self.tile_matrix.as_str())
            .join(self.tile_col.as_str())
    }
    fn filepath(&self, base_dir: &str) -> PathBuf {
        self.dirpath(base_dir).join(self.tile_row.as_str())
    }
    fn get_alias(&self) -> &str {
        self.alias.as_str()
    }
}

pub struct XyzRequest {
    pub alias: String,
    pub a: String,
    pub x: String,
    pub y: String,
    pub z: String,
}

impl XyzRequest {
    pub fn new(alias: &str, a: &str, x: &str, y: &str, z: &str) -> Self {
        XyzRequest {
            alias: String::from_str(alias).expect(""),
            a: String::from_str(a).expect(""),
            x: String::from_str(x).expect(""),
            y: String::from_str(y).expect(""),
            z: String::from_str(z).expect(""),
        }
    }
}
impl TileRequest for XyzRequest {
    fn dirpath(&self, base_dir: &str) -> PathBuf {
        Path::new(base_dir)
            .join(self.get_alias())
            .join(self.a.as_str())
            .join(self.x.as_str())
            .join(self.y.as_str())
    }
    fn filepath(&self, base_dir: &str) -> PathBuf {
        self.dirpath(base_dir).join(self.z.as_str())
    }
    fn get_alias(&self) -> &str {
        self.alias.as_str()
    }
}
