//! Configuration for TilesProxy, deserialized from TOML file.

use rocket::serde::Deserialize;
use std::fs;

/// Root configuration.
#[derive(Clone, Debug, Deserialize)]
#[serde(crate = "rocket::serde")]
pub struct Config {
    pub directory: String,
    pub port: usize,
    pub wmts: Vec<TileConfig>,
    pub xyz: Vec<TileConfig>,
}

/// Configuration of a tile server.
#[derive(Clone, Debug, Deserialize)]
#[serde(crate = "rocket::serde")]
pub struct TileConfig {
    /// Alias for the server.
    pub alias: String,
    /// Server URL template.
    pub url: String,
}

impl Config {
    /// Get config from a TOML file.
    pub fn from_file(filepath: &str) -> Config {
        let toml_str = fs::read_to_string(filepath)
            .unwrap_or_else(|_| panic!("Failed to read \"{}\".", filepath));

        toml::from_str(&toml_str)
            .unwrap_or_else(|_| panic!("Failed to deserialize \"{}\".", filepath))
    }
}
