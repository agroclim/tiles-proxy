# Tiles Proxy

TilesProxy: a cache proxy for tiles.

The aim of this little server is to act as a proxy for geographic tiles servers in order to limit calls to their services from our web applications.

Supported standards for tiles:
- [OGC](https://www.ogc.org/) WMTS: [OpenGIS Web Map Tile Service](https://www.ogc.org/standard/wmts/)
- XYZ (aka Slippy Map Tilenames): the de facto OpenStreetMap standard

Configuration of origin tiles servers is done in a TOML file.
See [`tiles-proxy.toml`](tiles-proxy.toml) for example.

To enable browser caching, HTTP headers are added: [ETag](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/ETag) and [Cache-Control](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cache-Control).
[CORS headers](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS) are also added to enable canvas export (eg.: in OpenLayers) from browser.

## Getting started

### Install Rust

*Tiles Proxy* uses the web framework for Rust: [Rocket](https://rocket.rs/).
Because of this, we'll need a recent release of Rust to run Rocket applications. If you already have a working installation of the latest Rust compiler, feel free to skip to the next section.

First, install `rustup`.

```sh
# install rustup, Proceed with standard installation
curl https://sh.rustup.rs -sSf | sh
# read the output to configure your shell
```

Once `rustup` is installed, ensure the latest toolchain is installed by running the command:

```sh
rustup default stable
```

Add pkg-config to compile openssl-sys

```sh
sudo apt install pkg-config
```

### Development

Format code with

```sh
cargo fmt --
```

Test with

```sh
cargo test
```

Use a linter with

```sh
cargo clippy
```

### Commits

Commit messages must follow the [Conventional Commits](https://www.conventionalcommits.org/) specification, a lightweight convention to write the commit message text.

### Configure VSCodium / VS Code

- [VSCodium](https://vscodium.com/)
- [Visual Studio Code](https://code.visualstudio.com/)

Recommended extensions:

- Even Better TOML (`ext install tamasfe.even-better-toml`):
  Fully-featured TOML support
  https://marketplace.visualstudio.com/items?itemName=tamasfe.even-better-toml
- rust-analyzer (`ext install rust-lang.rust-analyzer`):
  Rust language support for Visual Studio Code
  https://marketplace.visualstudio.com/items?itemName=rust-lang.rust-analyzer
- Conventional Commits (`ext install vivaxy.vscode-conventional-commits`)
  Conventional Commits for VSCode
  https://marketplace.visualstudio.com/items?itemName=vivaxy.vscode-conventional-commits

### Build

```sh
cargo build --release
```

Binary is at `target/release/tiles-proxy`.

### Run

```sh
cargo run tiles-proxy.toml
```

or

```sh
tiles-proxy tiles-proxy.toml
```
